# Copyright 2019 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

require pypi
require py-pep517 [ backend=hatchling entrypoints=[ ${PN} ] ]
require bash-completion
require zsh-completion

SUMMARY="Execute binaries from Python packages in isolated environments"
HOMEPAGE+=" https://pipxproject.github.io/pipx/"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/hatch_vcs[>=0.4][python_abis:*(-)?]
    build+run:
        dev-python/argcomplete[>=1.9.4][python_abis:*(-)?]
        dev-python/packaging[>=20.0][python_abis:*(-)?]
        dev-python/platformdirs[>=2.1.0][python_abis:*(-)?]
        dev-python/userpath[>=1.6.0][python_abis:*(-)?]
        python_abis:3.8? ( dev-python/tomli[python_abis:3.8?] )
        python_abis:3.9? ( dev-python/tomli[python_abis:3.9?] )
        python_abis:3.10? ( dev-python/tomli[python_abis:3.10?] )
"

# no tests in pypi distribution
RESTRICT="test"

BASH_COMPLETIONS=( "${FILES}/completion.sh" )
ZSH_COMPLETIONS=( "${FILES}/completion.sh _${PN}" )

src_install() {
    py-pep517_src_install
    bash-completion_src_install
    zsh-completion_src_install
}

